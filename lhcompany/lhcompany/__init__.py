__author__ = 'Matt Badger'

from google.appengine.ext import ndb


class Company(ndb.Expando):
    admin_user = ndb.StringProperty()
    company_name = ndb.StringProperty(indexed=False)
    primary_contact_first_name = ndb.StringProperty(indexed=False)
    primary_contact_last_name = ndb.StringProperty(indexed=False)
    primary_contact_email = ndb.StringProperty(indexed=False)
    secondary_contact_email = ndb.StringProperty(indexed=False)
    primary_contact_phone = ndb.StringProperty(indexed=False)
    primary_contact_mobile = ndb.StringProperty(indexed=False)
    years_trading = ndb.IntegerProperty(indexed=False)
    company_size = ndb.IntegerProperty(indexed=False)
    company_turnover = ndb.BooleanProperty(indexed=False)
    guild_membership_number = ndb.StringProperty(indexed=False)
    not_tax_registered = ndb.BooleanProperty(indexed=False)
    tax_number = ndb.StringProperty(indexed=False)
    twitter = ndb.StringProperty(indexed=False)
    facebook = ndb.StringProperty(indexed=False)
    website = ndb.StringProperty(indexed=False)
    address_1 = ndb.StringProperty(indexed=False)
    address_2 = ndb.StringProperty(indexed=False)
    address_3 = ndb.StringProperty(indexed=False)
    city = ndb.StringProperty(indexed=False)
    county = ndb.StringProperty(indexed=False)
    post_code = ndb.StringProperty(indexed=False)
    country = ndb.StringProperty(indexed=False, default='GB')
    voted = ndb.BooleanProperty(indexed=False, default=False)