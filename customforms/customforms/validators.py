# coding=utf-8
__author__ = 'Matt Badger'

import re
import wtforms
import locale_tools
from .constants import EMAIL_REGEXP, MOBILE_REGEXP, GUILD_MEMBERSHIP_NUMBER_REGEXP, GT15_ENTRY_CLASS_DICT, \
    WCA15_CLASS_DICT



# === Validators ===
def SecondaryEmailValidation(form, field):
    if len(field.data) > 0:
        regex = re.compile(EMAIL_REGEXP)
        if not regex.match(field.data or ''):
            raise wtforms.validators.ValidationError('Invalid characters found in email address')


def MobileValidator(form, field):
    if field.data:
        regex = re.compile(MOBILE_REGEXP)
        if not regex.match(field.data or ''):
            raise wtforms.validators.ValidationError('Invalid characters found in mobile number')


def UKCountyValidation(form, field):
    if form.country.data == 'GB' and not len(field.data) > 0:
        raise wtforms.validators.ValidationError('County is a required field for entrants in the UK.')
    elif form.country.data == 'GB' and field.data not in locale_tools.UK_COUNTY_SET:
        raise wtforms.validators.ValidationError('Please select a valid UK county from the list.')


def UKPostCodeValidation(form, field):
    if not len(field.data) > 0 and form.country.data == 'GB':
        raise wtforms.validators.ValidationError('Post code is a required field for entrants in the UK.')


def TaxRegisteredValidation(form, field):
    if form.country.data == 'GB':
        form.not_tax_registered.data = False
    elif not len(field.data) > 0 and locale_tools.eu_country(form.country.data):
        if not form.not_tax_registered.data:
            raise wtforms.validators.ValidationError('You must provide your tax number if you are tax registered.')


# def CountrySubdivisionValidation(form, field):
#     if len(field.data) > 0:
#         subdivision = pycountry.subdivisions.get(code=field.data)
#         if not subdivision:
#             raise validators.ValidationError('The county specified is invalid')
#         if not subdivision in list(pycountry.subdivisions.get(country_code=form.country.data)):
#             raise validators.ValidationError('The specified county is not valid for your chosen country.')

def DefaultSelectValidation(form, field):
    if field.data == 'default':
        raise wtforms.validators.ValidationError('Please select a valid option from the list.')


def CountryCodeValidation(form, field):
    if len(field.data) > 0:
        if not field.data in locale_tools.STATIC_COUNTRY_CODES_SET:
            raise wtforms.validators.ValidationError('The country you selected is not valid.')


def LanguageCodeValidation(form, field):
    if len(field.data) > 0:
        if not field.data in locale_tools.STATIC_LANGUAGE_CODES_SET:
            raise wtforms.validators.ValidationError('The language you selected is not valid.')


def GuildMembershipNumberValidation(form, field):
    if len(field.data) > 0:
        regex = re.compile(GUILD_MEMBERSHIP_NUMBER_REGEXP)
        if not regex.match(field.data or ''):
            raise wtforms.validators.ValidationError('Invalid membership number.')


def PromotionalCodeValidator(form, field):
    # if field.data and len(field.data) > 0 and field.data != 'PROD15GT':
    raise wtforms.validators.ValidationError('Invalid promotional code supplied. Codes are case sensitive.')


def CompanySizeValidator(form, field):
    try:
        field.data = int(field.data)
        if not field.data > 0:
            raise wtforms.validators.ValidationError('There must be at least 1 employee in the company.')
    except ValueError:
        raise wtforms.validators.ValidationError('Please enter a valid number. There must be at least 1 employee.')


def YearsTradingValidator(form, field):
    try:
        field.data = int(field.data)
    except ValueError:
        raise wtforms.validators.ValidationError('Please enter a valid number. There must be at least 1 employee.')


def EntryClassValidator(form, field):
    try:
        if field.data == 'default' or field.data == '':
            raise wtforms.validators.ValidationError('Please choose a valid entry category.')

        if not GT15_ENTRY_CLASS_DICT.get(field.data, False):
            raise wtforms.validators.ValidationError('Please choose a valid entry category.')
    except ValueError:
        raise wtforms.validators.ValidationError('Please choose a valid entry category.')


def EntryClassValidatorWCA(form, field):
    try:
        if field.data == 'default' or field.data == '':
            raise wtforms.validators.ValidationError('Please choose a valid entry category.')

        if not WCA15_CLASS_DICT.get(field.data, False):
            raise wtforms.validators.ValidationError('Please choose a valid entry category.')
    except ValueError:
        raise wtforms.validators.ValidationError('Please choose a valid entry category.')

