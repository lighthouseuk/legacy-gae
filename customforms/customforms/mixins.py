# coding=utf-8
import wtforms
from webapp2_extras.i18n import lazy_gettext as _
from .widgets import ExtendedSelectField
from .validators import SecondaryEmailValidation, LanguageCodeValidation, PromotionalCodeValidator, \
    UKPostCodeValidation, UKCountyValidation, CountryCodeValidation, YearsTradingValidator, CompanySizeValidator, \
    TaxRegisteredValidation, GuildMembershipNumberValidation, MobileValidator
from .constants import FIELD_MAXLENGTH, ADDRESS_MAXLENGTH, MOBILE_REGEXP, EMAIL_REGEXP, PHONE_MAXLENGTH, CITY_MAXLENGTH
import locale_tools
import re

__author__ = 'Matt Badger'

# === Filters ===
strip_filter = lambda x: x.replace(" ", "") if x else None


# ==== Mixins ====
class ReferrerMixin(object):
    referrer = wtforms.fields.HiddenField(_('referrer'))


class EntityKeyMixin(object):
    key = wtforms.fields.HiddenField(validators=[wtforms.validators.InputRequired(), ])


class PasswordMixin(object):
    password = wtforms.fields.PasswordField(_('Password'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=FIELD_MAXLENGTH, message=_(
            "Field cannot be longer than %(max)d characters."))
    ])


class PasswordConfirmMixin(PasswordMixin):
    # password = wtforms.fields.StringField(_('Password'), [
    #     wtforms.validators.InputRequired(),
    #     wtforms.validators.Length(max=FIELD_MAXLENGTH, message=_(
    #         "Field cannot be longer than %(max)d characters."))
    # ])
    c_password = wtforms.fields.PasswordField(_('Confirm Password'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.EqualTo('password', _('Passwords must match.')),
        wtforms.validators.Length(max=FIELD_MAXLENGTH,
                                  message=_("Field cannot be longer than %(max)d characters."))
    ])


class UsernameMixin(object):
    username = wtforms.fields.StringField(_('Username or Email Address'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=FIELD_MAXLENGTH, message=_(
            "Field cannot be longer than %(max)d characters.")),
    ])


class LanguagePreference(object):
    language_preference = wtforms.fields.SelectField(_('Preferred Language'), [
        # custom validator to check that value is in list specified.
        LanguageCodeValidation,
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=2, message=_("Language code must be %(max)d characters.")),
    ], choices=locale_tools.STATIC_LANGUAGE_CODES_TUPLE, default='en')


class NameMixin(object):
    first_name = wtforms.fields.StringField(_('First Name'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=FIELD_MAXLENGTH, message=_("Field cannot be longer than %(max)d characters.")),
    ])
    last_name = wtforms.fields.StringField(_('Last Name'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=FIELD_MAXLENGTH, message=_("Field cannot be longer than %(max)d characters.")),
    ])


class DisplayNameMixin(object):
    first_name = wtforms.fields.StringField(_('First Name'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=20, message=_("First name cannot be longer than %(max)d characters.")),
    ])
    last_name = wtforms.fields.StringField(_('Last Name'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=20, message=_("Last name cannot be longer than %(max)d characters.")),
    ])


class EmailMixin(object):
    email = wtforms.fields.StringField(_('Email'), [wtforms.validators.InputRequired(),
                                                    wtforms.validators.Length(max=FIELD_MAXLENGTH,
                                                                              message=_(
                                                                                  "Email address must be less than between %(max)d characters long.")),
                                                    wtforms.validators.regexp(EMAIL_REGEXP,
                                                                              message=_(
                                                                                  'Invalid characters found in email address.'))])


class EmailAddressMixin(object):
    email_address = wtforms.fields.StringField(_('Email'), [wtforms.validators.InputRequired(),
                                                            wtforms.validators.Length(
                                                                max=FIELD_MAXLENGTH,
                                                                message=_(
                                                                    "Email address must be less than between %(max)d characters long.")),
                                                            wtforms.validators.regexp(EMAIL_REGEXP, flags=re.UNICODE,
                                                                                      message=_(
                                                                                          'Invalid characters found in email address.'))])


class TermsAgreedMixin(object):
    terms_agreed = wtforms.fields.BooleanField(
        _('I confirm that I have read the terms and conditions.'),
        [wtforms.validators.Required(_('Please tick to confirm that you have read the terms and conditions of entry.'))]
    )


class PromoCodeMixin(object):
    promo_code = wtforms.fields.StringField(
        _(u'Promotional code: '),
        [PromotionalCodeValidator]
    )


class OrderSummaryConfirmedMixin(object):
    summary_confirmed = wtforms.fields.BooleanField(
        _('I confirm that I have added all of my entries and reviewed my order.'),
        [wtforms.validators.Required(_('Please tick to confirm that you have reviewed your order.'))]
    )


class MobilePhoneMixin(object):
    mobile = wtforms.fields.StringField(
        _('Mobile'),
        [
            wtforms.validators.Length(min=8, max=15, message=_(
                "Mobile number must be between %(min)d and %(max)d characters long.")),
            wtforms.validators.regexp(MOBILE_REGEXP,
                                      message=_('Invalid characters found in mobile number.'))],
        filters=[strip_filter])


class PhoneMixin(object):
    phone = wtforms.fields.StringField(
        _('Phone'),
        [
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(max=PHONE_MAXLENGTH, message=_(
                "Phone number must be less than %(max)d characters long.")),
            wtforms.validators.regexp(MOBILE_REGEXP,
                                      message=_('Invalid characters found in phone number.'))],
        filters=[strip_filter])


class AddressMixin(object):
    address_1 = wtforms.fields.StringField(_('Address 1'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=ADDRESS_MAXLENGTH,
                                  message=_("Address 1 cannot be longer than %(max)d characters.")),
    ])
    address_2 = wtforms.fields.StringField(_('Address 2'), [
        wtforms.validators.Length(max=ADDRESS_MAXLENGTH,
                                  message=_("Address 2 cannot be longer than %(max)d characters.")),
    ])
    address_3 = wtforms.fields.StringField(_('Address 3'), [
        wtforms.validators.Length(max=ADDRESS_MAXLENGTH,
                                  message=_("Address 3 cannot be longer than %(max)d characters.")),
    ])
    city = wtforms.fields.StringField(_('City'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=CITY_MAXLENGTH,
                                  message=_("City cannot be longer than %(max)d characters.")),
    ])
    state = wtforms.fields.StringField(_('County/State'), [
        # In theory county will now be an alphanumeric code so length is not required
        # Need to check that US states work when passing to SagePay
        wtforms.validators.Length(max=CITY_MAXLENGTH,
                                  message=_("County/State cannot be longer than %(max)d characters.")),
    ])
    county = ExtendedSelectField(_('County (Required for UK companies)'), [
        UKCountyValidation,
    ], choices=locale_tools.UK_COUNTIES_TUPLE)
    post_code = wtforms.fields.StringField(_('Post Code/Zip Code (Required for UK companies)'), [
        UKPostCodeValidation,
    ])
    country = wtforms.fields.SelectField(_('Country'), [
        # custom validator to check that value is in list specified.
        CountryCodeValidation,
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=2, message=_("Country code must be %(max)d characters.")),
    ], choices=locale_tools.STATIC_COUNTRY_LABLES_TUPLE, default='GB')


class CompanyMixin(object):
    company_name = wtforms.fields.StringField(_('Company Name'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=ADDRESS_MAXLENGTH,
                                  message=_("Company name cannot be longer than %(max)d characters.")),
    ])
    years_trading = wtforms.fields.StringField(_('Number of years trading. (To the nearest year)'), [
        wtforms.validators.InputRequired(),
        YearsTradingValidator,
    ])
    company_size = wtforms.fields.StringField(_('Number of employees'), [
        wtforms.validators.InputRequired(),
        CompanySizeValidator,
    ])
    company_turnover = wtforms.fields.BooleanField(_(u'Our company turnover is less than £1million'), [
    ])
    not_tax_registered = wtforms.fields.BooleanField(_('We are not VAT/TVA/IVA registered'), [])
    tax_number = wtforms.fields.StringField(_('VAT/TVA/IVA Number'), [
        TaxRegisteredValidation,
    ])


class CompanyContactMixin(object):
    primary_contact_first_name = wtforms.fields.StringField(_('Primary Contact First Name'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=20, message=_("First name cannot be longer than %(max)d characters.")),
    ])
    primary_contact_last_name = wtforms.fields.StringField(_('Primary Contact Last Name'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=20, message=_("Last name cannot be longer than %(max)d characters.")),
    ])
    primary_contact_email = wtforms.fields.StringField(_('Primary Contact Email'), [wtforms.validators.InputRequired(),
                                                                                    wtforms.validators.Length(
                                                                                        max=FIELD_MAXLENGTH,
                                                                                        message=_(
                                                                                            "Email address must be less than between %(max)d characters long.")),
                                                                                    wtforms.validators.regexp(
                                                                                        EMAIL_REGEXP,
                                                                                        message=_(
                                                                                            'Invalid characters found in primary contact email address.'))])
    secondary_contact_email = wtforms.fields.StringField(_('Secondary Contact Email'),
                                                         [SecondaryEmailValidation])
    primary_contact_phone = wtforms.fields.StringField(
        _('Primary Contact Telephone'),
        [
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(max=PHONE_MAXLENGTH, message=_(
                "Phone number must be less than %(max)d characters long.")),
            wtforms.validators.regexp(MOBILE_REGEXP,
                                      message=_('Invalid characters found in phone number.'))],
        filters=[strip_filter])
    primary_contact_mobile = wtforms.fields.StringField(_('Primary Contact Mobile'), [MobileValidator],
                                                        filters=[strip_filter])


class CompanyMembershipMixin(object):
    # Override the name of this in the form to a client specific varient
    guild_membership_number = wtforms.fields.StringField(_('Membership Number'), [
        GuildMembershipNumberValidation,
    ])


class TwitterMixin(object):
    twitter = wtforms.fields.StringField(_('Twitter'), [
    ])


class FacebookMixin(object):
    facebook = wtforms.fields.StringField(_('Facebook'), [
    ])


class WebsiteMixin(object):
    website = wtforms.fields.StringField(_('Website'), [
    ])


class SocialMediaMixin(FacebookMixin, TwitterMixin, WebsiteMixin):
    pass


# class ContactMixin(DisplayNameMixin, MobilePhoneMixin, EmailMixin):
class ContactMixin(DisplayNameMixin, EmailMixin):
    pass


class SearchField(object):
    query = wtforms.fields.StringField(_('Search'), [
        wtforms.validators.InputRequired(),
    ])
