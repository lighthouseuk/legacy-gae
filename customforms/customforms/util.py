__author__ = 'Matt Badger'


def form_to_obj(form, target_object):

    """
    Populates the attributes of the passed `obj` with data from the form's
    fields.

    :note: This is a destructive operation; Any attribute with the same name
           as a field will be overridden. Use with caution.
    """
    for name in target_object._properties:
        if hasattr(form, name):
            setattr(target_object, name, getattr(form, name).data)

    # return object