__author__ = 'Matt Badger'

import wtforms
import wtforms.csrf.session as csrf_lib
import datetime
import webapp2_extras.i18n as i18n


class FormTranslations(object):
    def gettext(self, string):
        return i18n.gettext(string)

    def ngettext(self, singular, plural, n):
        return i18n.ngettext(singular, plural, n)


class BaseForm(wtforms.Form):
    action_url = ''
    title = ''

    class Meta:
        csrf = True
        csrf_class = csrf_lib.SessionCSRF
        csrf_secret = b'T1rJxBybnt^3^#HRZjM$mMR$0z$&A3'
        csrf_time_limit = datetime.timedelta(minutes=720)

    def __init__(self, request, formdata=None, existing_obj=None, data=None, **kwargs):
        if not formdata:
            formdata = request.POST
        super(BaseForm, self).__init__(formdata=formdata, obj=existing_obj, data=data, meta={'csrf_context': request.session}, **kwargs)

    def _get_translations(self):
        return FormTranslations()

