import wtforms
import wtforms.fields.html5 as html5
from .base import BaseForm
from .widgets import ExtendedSelectField
from .mixins import UsernameMixin, PasswordMixin, EmailMixin, PasswordConfirmMixin, ReferrerMixin, EntityKeyMixin, NameMixin, LanguagePreference, CompanyMixin, CompanyContactMixin, CompanyMembershipMixin, AddressMixin, SocialMediaMixin, SearchField, PhoneMixin, PromoCodeMixin, EmailAddressMixin
from .validators import EntryClassValidator, EntryClassValidatorWCA, DefaultSelectValidation
from .constants import FIELD_MAXLENGTH, ADDRESS_MAXLENGTH, GT15_ENTRY_CLASS_TUPLE, WCA15_CLASS_TUPLE, WC14_ENTRY_CLASS_LIST
from webapp2_extras.i18n import lazy_gettext as _


__author__ = 'Matt Badger'


class LoginForm(BaseForm, UsernameMixin, PasswordMixin):
    class Meta:
        csrf = False

    title = 'Login Form'


class SendVerifyTokenForm(BaseForm, EmailMixin):
    class Meta:
        csrf = False

    title = 'Send Verification Form'


class SendVerifyTokenUsernameForm(BaseForm, UsernameMixin):
    class Meta:
        csrf = False

    title = 'Send Verification By UsernameForm'


class ResetPasswordForm(BaseForm, PasswordConfirmMixin):
    class Meta:
        csrf = False
    title = 'Password Reset Form'

    token = wtforms.fields.HiddenField(validators=[wtforms.validators.InputRequired()])


class ChangePasswordForm(BaseForm, PasswordConfirmMixin):
    current_password = wtforms.fields.PasswordField(_('Current Password'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=FIELD_MAXLENGTH, message=_(
            "Field cannot be longer than %(max)d characters."))
    ])


class AdminChangePasswordForm(BaseForm, ReferrerMixin, EntityKeyMixin, PasswordConfirmMixin):
    pass


class UserRegistrationForm(BaseForm, NameMixin, UsernameMixin, EmailMixin, PasswordConfirmMixin, LanguagePreference):
    class Meta:
        csrf = False

    title = 'User Registration Form'


class UserSecurityProfileForm(BaseForm, ReferrerMixin, EntityKeyMixin, NameMixin, UsernameMixin, EmailAddressMixin, LanguagePreference):
    title = 'User Profile Form'


class UserForm(BaseForm):
    pass


class SystemRoles(wtforms.Form):
    pass


def build_dynamic_profile_form(request, formdata=None, existing_obj=None, data=None):
    form = UserSecurityProfileForm

    form_instance = form(request=request, formdata=formdata, data=data, existing_obj=existing_obj)
    # TODO: add link to reset password
    del form_instance.username
    # del form_instance.email_address
    return form_instance


def build_dynamic_admin_profile_form(request, formdata=None, existing_obj=None, data=None, group_list=None, current_user_group_list=None):
    form = UserSecurityProfileForm

    if group_list:
        sys_roles = SystemRoles
        user_roles = {}
        for group in group_list:
            setattr(sys_roles, group, wtforms.fields.BooleanField(_(group.capitalize()), validators=[]))
            if current_user_group_list:
                user_roles[group] = True if group in current_user_group_list else False

        setattr(form, u'system_roles', wtforms.FormField(sys_roles))
        if existing_obj:
            setattr(existing_obj, u'system_roles', user_roles)

    form_instance = form(request=request, formdata=formdata, data=data, existing_obj=existing_obj)

    # TODO: add reset password fields
    # TODO: add reset password link
    del form_instance.username
    # del form_instance.email_address
    return form_instance

# TODO: Office/branch form


class CompanyForm(BaseForm, CompanyMixin):
    pass


class ConsolidatedCompanyForm(BaseForm, ReferrerMixin, EntityKeyMixin, CompanyMixin, CompanyContactMixin, AddressMixin, CompanyMembershipMixin, SocialMediaMixin):
    pass


class ChooseCompanyForm(BaseForm):
    # TODO: select menu with company_id=>name combos
    pass


class CompanyUserForm(BaseForm):
    # If company id does not match a company that the user is part of (unless admin) then block the request
    # TODO: company (select menu that gets disabled/not rendered on user side
    # Email is probably the easiest, rather than remembering the username or an id
    # TODO: user_email
    # or
    # TODO: user_id
    # or
    # TODO: user_name

    # TODO: system group (only available to admins)
    # TODO: permissions
    pass


class UserSearchForm(BaseForm, SearchField):
    class Meta:
        csrf = False


class GoodsInSearchForm(BaseForm, SearchField):
    class Meta:
        csrf = False


class TermsForm(BaseForm):
    read = wtforms.fields.BooleanField(_('Please tick to confirm that you have read the terms and conditions'), validators=[wtforms.validators.InputRequired()])


class MockRequestCSRF(BaseForm):
    item_key = wtforms.fields.HiddenField(validators=[wtforms.validators.InputRequired(),])


class SagePayRequestForm(wtforms.Form):
    VPSProtocol = wtforms.fields.HiddenField()
    TxType = wtforms.fields.HiddenField()
    Vendor = wtforms.fields.HiddenField()
    Crypt = wtforms.fields.HiddenField()
    pay = wtforms.fields.SubmitField('Pay')


class EntryForm(BaseForm):
    title = 'Entry Form'
    basket_item_key = wtforms.HiddenField()
    entry_class = ExtendedSelectField(
        _('Category Number'),
        [
            # validators.Required(),
            EntryClassValidator,
        ],
        choices=GT15_ENTRY_CLASS_TUPLE,
        # widget=SelectOptGroups()
    )
    entry_name = wtforms.fields.StringField(_('Product Name'), [
        wtforms.validators.InputRequired(),
        # validate length
    ])
    supermarket_own_brand = wtforms.fields.BooleanField(
        _('Please tick this box if this entry is a supermarket own brand'), [])
    available_in_supermarket = wtforms.fields.BooleanField(
        _('Please tick this box if this entry is available to purchase in a supermarket'), [])
    entry_description = wtforms.fields.StringField(
        _('Product Description (<strong>Factual</strong>. <strong>NO Brand names</strong>. Max 150 Characters.)'), [
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(max=150, message=_(
                "Product Description must be less than %(max)d characters long.")),
        ])
    entry_ingredients = wtforms.fields.StringField(
        _('Product ingredients (Max 200 Characters)'), [
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(max=200, message=_(
                "Product Ingredient list must be less than %(max)d characters long.")),
        ])
    entry_allergens = wtforms.fields.BooleanField(
        _('Please tick this box if any of your ingredients are listed on the <a href="http://storage.googleapis.com/guild-fine-food/gt15/Top%2014%20Allergens.pdf" target="_blank">allergy advice guide</a>'), [])
    entry_producer = wtforms.fields.StringField(_('Producer/Manufacturer'), [
        wtforms.validators.InputRequired(),
        # validate length
    ])
    entry_storage = wtforms.fields.RadioField(
        _('Product Storage'),
        [
            wtforms.validators.InputRequired(),
            # validate length
        ],
        choices=[
            ('F',_('Frozen')),
            ('C',_('Chilled')),
            ('A',_('Ambient')),
        ],
    )


class EntryFormWCA(EntryForm):
    entry_class = ExtendedSelectField(
        _('Category Number'),
        [
            # validators.Required(),
            EntryClassValidatorWCA,
        ],
        choices=WCA15_CLASS_TUPLE,
        # widget=SelectOptGroups()
    )


class BasketPromoCodeForm(BaseForm, PromoCodeMixin):
    title = 'Promotional Code'


class UserDetailsForm(BaseForm, NameMixin):
    pass


class PasswordResetForm(BaseForm, PasswordConfirmMixin):
    pass


class WC14PublicResultsSearch(wtforms.Form):
    category_id = ExtendedSelectField(
        _('Category'),
        [
        ],
        choices=WC14_ENTRY_CLASS_LIST,
        # widget=SelectOptGroups()
    )
    product_name = wtforms.fields.StringField(_('Product Name'), [
        # validate length
    ])
    company_name = wtforms.fields.StringField(_('Company'), [
        # validate length
    ])
    award = wtforms.fields.SelectField(
        _('Award'),
        [
        ],
        choices=[
            (u'All', u'All'),
            (u'Super Gold', u'Super Gold'),
            (u'Gold', u'Gold'),
            (u'Silver', u'Silver'),
            (u'Bronze', u'Bronze'),
        ],
    )


class UploadForm(BaseForm):
    upload_file = wtforms.fields.FileField(_(u'Please select CSV file to import:'), validators=[])


class DeleteUploadForm(BaseForm):
    file_name = wtforms.fields.HiddenField(validators=[wtforms.validators.InputRequired()])


class DeleteEntityForm(BaseForm, EntityKeyMixin):
    pass


class DeliveryLocationForm(BaseForm, EntityKeyMixin, PhoneMixin, AddressMixin):
    location_ref = wtforms.fields.StringField(_('Location Ref'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=ADDRESS_MAXLENGTH,
                                  message=_("Company name cannot be longer than %(max)d characters.")),
    ])
    company_name = wtforms.fields.StringField(_('Company Name'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=ADDRESS_MAXLENGTH,
                                  message=_("Company name cannot be longer than %(max)d characters.")),
    ])
    contact_name = wtforms.fields.StringField(_('Contact Name'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=50, message=_("Contact name cannot be longer than %(max)d characters.")),
    ])
    notes = wtforms.fields.TextAreaField(_('Notes (displayed to user)'), [
    ])


class DeliveryScheduleForm(BaseForm, EntityKeyMixin):
    schedule_ref = wtforms.fields.StringField(_('Ref. Code (e.g. GUILDHOUSE; cannot be modified)'), [
        wtforms.validators.InputRequired(),
    ])
    notes = wtforms.fields.TextAreaField(_('Notes'), [
    ])
    delivery_date = html5.DateField(_('Delivery Date'), validators=[wtforms.validators.InputRequired()])
    judging_date = html5.DateField(_('Judging Date'), validators=[wtforms.validators.InputRequired()])
    jit = wtforms.fields.BooleanField(_('JIT Delivery Schedule?'), validators=[])


def build_delivery_schedule_form(request, formdata=None, existing_obj=None, data=None, locations_tuple=None):
    form = DeliveryScheduleForm

    if locations_tuple:
        form.location = wtforms.fields.SelectField(_('Delivery Location:'), [wtforms.validators.InputRequired(), DefaultSelectValidation],
                                                    choices=locations_tuple, )

    form_instance = form(request=request, formdata=formdata, data=data, existing_obj=existing_obj)

    return form_instance


class DatapipelineSelectorForm(BaseForm):
    pass


def build_datapipeline_selector(request, formdata=None, existing_obj=None, data=None, pipelines_tuple=None):
    form = DatapipelineSelectorForm

    if pipelines_tuple:
        form.datapipeline = ExtendedSelectField(_('Select job:'), [wtforms.validators.InputRequired()],
            choices=pipelines_tuple,
        )

    form_instance = form(request=request, formdata=formdata, data=None, existing_obj=existing_obj)

    return form_instance


class DeliveryFormExistingData(object):
    pass


def build_dynamic_entry_delivery_form(request, entries, company_id, formdata=None, existing_obj=None, data=None):
    class CompanyEntryDeliveryForm(BaseForm, EntityKeyMixin):
        pass

    form = CompanyEntryDeliveryForm
    existing_data_obj = DeliveryFormExistingData()
    setattr(existing_data_obj, u'key', company_id)

    for entry in entries:
        setattr(existing_data_obj, u'{}_notes'.format(entry.security_meta.uid), entry.delivery.notes)
        setattr(existing_data_obj, u'{}_delivered'.format(entry.security_meta.uid), True if entry.delivery.status == 'DELIVERED' else False)

        setattr(form, u'{}_notes'.format(entry.security_meta.uid), wtforms.fields.TextAreaField(_('Notes'), validators=[wtforms.validators.Length(max=495, message=_("Notes must be less than %(max)d characters long.")),]))
        setattr(form, u'{}_delivered'.format(entry.security_meta.uid), wtforms.fields.BooleanField(_('Delivered'), validators=[]))

    form_instance = form(request=request, formdata=formdata, data=None, existing_obj=existing_data_obj)

    return form_instance


class ScheduleFormExistingData(object):
    pass


def build_dynamic_entry_schedule_form(request, entries, company_id, schedule_tuple, status_tuple, formdata=None, existing_obj=None, data=None):
    class CompanyEntryScheduleForm(BaseForm, EntityKeyMixin):
        pass

    form = CompanyEntryScheduleForm
    existing_data_obj = ScheduleFormExistingData()
    setattr(existing_data_obj, u'key', company_id)

    for entry in entries:
        setattr(existing_data_obj, u'{}_schedule'.format(entry.security_meta.uid), entry.delivery.schedule_data.get('schedule_key', u'default') or u'default')
        setattr(existing_data_obj, u'{}_status'.format(entry.security_meta.uid), entry.delivery.status)

        setattr(form, u'{}_schedule'.format(entry.security_meta.uid), ExtendedSelectField(_('Delivery Schedule:'), [], choices=schedule_tuple,))
        setattr(form, u'{}_status'.format(entry.security_meta.uid), wtforms.fields.SelectField(_('Status'), validators=[wtforms.validators.InputRequired()], choices=status_tuple))

    form_instance = form(request=request, formdata=formdata, data=data, existing_obj=existing_data_obj)

    return form_instance


class EntryDetailsFormExistingData(object):
    pass


def build_dynamic_entry_details_form(request, entries, company_id, formdata=None, existing_obj=None, data=None):
    class CompanyEntryDetailsForm(BaseForm, EntityKeyMixin):
        pass

    form = CompanyEntryDetailsForm
    existing_data_obj = EntryDetailsFormExistingData()
    setattr(existing_data_obj, u'key', company_id)

    storage_choices = [(u'F', _(u'Frozen')), (u'C', _(u'Chilled')), (u'A', _(u'Ambient'))]

    for entry in entries:
        setattr(existing_data_obj, u'{}_entry_name'.format(entry.security_meta.uid), entry.entry_name)
        setattr(existing_data_obj, u'{}_supermarket_own_brand'.format(entry.security_meta.uid), entry.supermarket_own_brand)
        setattr(existing_data_obj, u'{}_available_in_supermarket'.format(entry.security_meta.uid), entry.available_in_supermarket)
        setattr(existing_data_obj, u'{}_entry_description'.format(entry.security_meta.uid), entry.entry_description)
        setattr(existing_data_obj, u'{}_entry_ingredients'.format(entry.security_meta.uid), entry.entry_ingredients)
        setattr(existing_data_obj, u'{}_entry_allergens'.format(entry.security_meta.uid), entry.entry_allergens)
        setattr(existing_data_obj, u'{}_entry_producer'.format(entry.security_meta.uid), entry.entry_producer)
        setattr(existing_data_obj, u'{}_entry_storage'.format(entry.security_meta.uid), entry.entry_storage)

        setattr(form, u'{}_entry_name'.format(entry.security_meta.uid), wtforms.fields.StringField(_(u'Name:'), [wtforms.validators.InputRequired()]))
        setattr(form, u'{}_supermarket_own_brand'.format(entry.security_meta.uid), wtforms.fields.BooleanField(_(u'S.M.O.B:'), validators=[]))
        setattr(form, u'{}_available_in_supermarket'.format(entry.security_meta.uid), wtforms.fields.BooleanField(_(u'A.I.S:'), validators=[]))
        setattr(form, u'{}_entry_description'.format(entry.security_meta.uid), wtforms.fields.TextAreaField(_(u'Description:'), [wtforms.validators.InputRequired()]))
        setattr(form, u'{}_entry_ingredients'.format(entry.security_meta.uid), wtforms.fields.TextAreaField(_(u'Ingredients:'), [wtforms.validators.InputRequired()]))
        setattr(form, u'{}_entry_allergens'.format(entry.security_meta.uid), wtforms.fields.BooleanField(_(u'Allergens:'), validators=[]))
        setattr(form, u'{}_entry_producer'.format(entry.security_meta.uid), wtforms.fields.StringField(_(u'Producer:'), [wtforms.validators.InputRequired()]))
        setattr(form, u'{}_entry_storage'.format(entry.security_meta.uid), wtforms.fields.SelectField(_(u'Storage:'), validators=[wtforms.validators.InputRequired()], choices=storage_choices))


    form_instance = form(request=request, formdata=formdata, data=data, existing_obj=existing_data_obj)

    return form_instance


class JoinJudgingSessionForm(BaseForm, EntityKeyMixin):
    # Must pass the session key to this form
    table = wtforms.fields.StringField(_('Table (number or name)'), [
        wtforms.validators.InputRequired(),
    ])


class JudgingSearchForm(BaseForm, SearchField):
    class Meta:
        csrf = False


class JudgingCommentForm(BaseForm, EntityKeyMixin):
    comment = wtforms.fields.TextAreaField(_('Comments'), [
        wtforms.validators.InputRequired(),
    ])
    rating = wtforms.fields.IntegerField(_('Rating (0-3)'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.NumberRange(min=0, max=3, message='Please enter a rating between 0 and 3'),
    ])


class StartJudgingSessionForm(BaseForm):
    ref = wtforms.fields.StringField(_('Session Reference (to help judges pick the correct session e.g. Guild House)'), [wtforms.validators.InputRequired(),])
    notes = wtforms.fields.TextAreaField(_('Notes (optional)'), [])


class EndJudgingSessionForm(BaseForm, EntityKeyMixin):
    confirm = wtforms.fields.BooleanField(_('I confirm that I want to end this judging session'), validators=[wtforms.validators.InputRequired()])


def build_judging_session_form(request, formdata=None, existing_obj=None, data=None, locations_tuple=None):
    form = StartJudgingSessionForm

    if locations_tuple:
        form.location = wtforms.fields.SelectField(_('Location:'), [wtforms.validators.InputRequired(), DefaultSelectValidation],
                                                    choices=locations_tuple, )

    form_instance = form(request=request, formdata=formdata, data=data, existing_obj=existing_obj)

    return form_instance


class JudgingMasterForm(BaseForm, EntityKeyMixin):
    comments = wtforms.fields.TextAreaField(_('Reviewed Comments'), [
        wtforms.validators.InputRequired(),
    ])
    rating = wtforms.fields.IntegerField(_('Review Rating'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.NumberRange(min=0, max=3, message='Please enter a rating between 0 and 3'),
    ])
    editing_locked = wtforms.fields.BooleanField(_('Editing Locked'), validators=[])
    reviewed = wtforms.fields.BooleanField(_('Reviewed'), validators=[])
    finalised = wtforms.fields.BooleanField(_('Finalised'), validators=[])


class ShopOfTheYear(BaseForm):
    shop_name = wtforms.fields.StringField(_('Shop Name *'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=100, message=_("Shop name cannot be longer than %(max)d characters.")),
    ])
    shop_city = wtforms.fields.StringField(_('Town/City *'), [
        wtforms.validators.InputRequired(),
        wtforms.validators.Length(max=100, message=_("Town/City cannot be longer than %(max)d characters.")),
    ])
    shop_post_code = wtforms.fields.StringField(_('Post Code'), [])


class ResultsAdmin(BaseForm):
    block_results = wtforms.fields.BooleanField(_('Block results from company'), [])
    soty_override = wtforms.fields.BooleanField(_('Skip SOTY vote for company'), [])


class CacheDebug(BaseForm):
    cache_key = wtforms.fields.StringField(_('Cache Key'), [
        wtforms.validators.InputRequired(),
    ])
