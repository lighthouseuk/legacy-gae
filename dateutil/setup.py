# Always prefer setuptools over distutils
from setuptools import setup

setup(
    name='dateutil',
    version='1.5.0',

    description="Extensions to the standard Python datetime module",
    long_description=
    """\
    The dateutil module provides powerful extensions to the
    datetime module available in the Python standard library.
    """,

    # The project's main homepage.
    url="http://labix.org/python-dateutil",

    # Author details
    author="Tomi Pievilaeinen",
    author_email="tomi.pievilainen@iki.fi",

    # Choose your license
    license="Simplified BSD",

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Topic :: Software Development :: Libraries',
    ],

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=["dateutil", "dateutil.zoneinfo"],
    package_data={"": ["*.tar.gz"]},
    include_package_data=True,
    zip_safe=False,
    requires=["six"],
    install_requires=['six'],
)
