__author__ = 'Matt Badger'

import random
import hashlib
import logging
import string
import config


def random_string(size=6, chars=string.ascii_letters + string.digits):
    """ Generate random string """
    return ''.join(random.choice(chars) for _ in range(size))


def hashing(plaintext, salt="", sha="512"):
    """ Returns the hashed and encrypted hexdigest of a plaintext and salt"""
    # app = webapp2.get_app()

    # Hashing
    if sha == "1":
        phrase = hashlib.sha1()
    elif sha == "256":
        phrase = hashlib.sha256()
    else:
        phrase = hashlib.sha512()
    phrase.update("%s@%s" % (plaintext, salt))
    phrase_digest = phrase.hexdigest()

    # Encryption (PyCrypto)
    # wow... it's so secure :)
    try:
        from Crypto.Cipher import AES

        mode = AES.MODE_CBC

        # We can not generate random initialization vector because is difficult to retrieve them later without knowing
        # a priori the hash to match. We take 16 bytes from the hexdigest to make the vectors different for each hashed
        # plaintext.
        iv = phrase_digest[:16]
        encryptor = AES.new(config.settings.get(config.ENCRYPTION_SETTINGS, 'aes_key'), mode, iv)
        ciphertext = [encryptor.encrypt(chunk) for chunk in chunks(phrase_digest, 16)]
        return ''.join(ciphertext)
    except Exception, e:
        logging.error("CRYPTO is not running: {}".format(e))
        raise


def chunks(list, size):
    """ Yield successive sized chunks from list. """

    for i in xrange(0, len(list), size):
        yield list[i:i + size]


def encode(plainText):
    num = 0
    key = "0123456789abcdefghijklmnopqrstuvwxyz"
    key += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    for c in plainText: num = (num << 8) + ord(c)
    encodedMsg = ""
    while num > 0:
        encodedMsg = key[num % len(key)] + encodedMsg
        num /= len(key)
    return encodedMsg


def decode(encodedMsg):
    num = 0
    key = "0123456789abcdefghijklmnopqrstuvwxyz"
    key += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    for c in encodedMsg: num = num * len(key) + key.index(c)
    text = ""
    while num > 0:
        text = chr(num % 256) + text
        num /= 256
    return text