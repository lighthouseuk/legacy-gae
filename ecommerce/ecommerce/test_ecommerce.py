__author__ = 'Matt Badger'

import unittest

import config.ecommerce_price_modifiers as price_mods
from satchless.item import Item as satchlessitem

try:
    import ecommerce.ecommerce.base_price as baseprice
    import ecommerce.ecommerce.inventory as inventory
    import ecommerce.ecommerce.basket as bskt
    import extlib.ecommerce.base_modifiers as mods
except ImportError:
    import extlib.ecommerce.base_price as baseprice
    import extlib.ecommerce.inventory as inventory
    import extlib.ecommerce.basket as bskt
    import extlib.ecommerce.base_modifiers as mods


class UncustomisableItem(satchlessitem):
    def get_price_per_item(self, **kwargs):
        return baseprice.Price(net=30, currency='GBP')


class UncustomisableItemTwo(satchlessitem):
    def get_price_per_item(self, **kwargs):
        return baseprice.Price(net=55, currency='GBP')


UNCUSTOMISABLE_ITEM_ONE = UncustomisableItem()
UNCUSTOMISABLE_ITEM_TWO = UncustomisableItemTwo()
NORMAL_ENTRY = inventory.get_inventory_item('2101')
MEALKIT_ENTRY = inventory.get_inventory_item('3301')


EXAMPLE_CUSTOMISATION_ONE = NORMAL_ENTRY.create_customisation('Class 101', 'Entry Name', 'Entry Description', 'Ingredient1, ingredient2, ingredient3', 'Allergens', 'Producer', 'Frozen')
EXAMPLE_CUSTOMISATION_TWO = NORMAL_ENTRY.create_customisation('Class 3301', 'Mealkit Entry', 'Description', 'Ingredient1, ingredient2, ingredient3', 'Allergens', 'Producer', 'Frozen')
EXAMPLE_CUSTOMISATION_THREE = NORMAL_ENTRY.create_customisation('Class 2101', 'Edited Entry', 'Edited Description', 'Ingredient1, ingredient2, ingredient3', 'Allergens', 'Producer', 'Frozen')


class BasketTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_basket_multiple_same_product(self):
        basket = bskt.Basket()
        basket.add(product=UNCUSTOMISABLE_ITEM_ONE)

        basket.add(product=UNCUSTOMISABLE_ITEM_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 2, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(60, currency='GBP'), 'Basket total incorrect')

    def test_basket_multiple_different_products(self):
        basket = bskt.Basket()
        basket.add(product=UNCUSTOMISABLE_ITEM_ONE)
        basket.add(product=UNCUSTOMISABLE_ITEM_TWO)

        self.assertEqual(len(basket), 2, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 2, 'Basket item count incorrect')
        self.assertEqual(basket.get_total().gross, baseprice.Price(85, currency='GBP').gross, 'Basket total incorrect')

    def test_basket_add_custom_product(self):
        basket = bskt.Basket()
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(77, currency='GBP'), 'Basket total incorrect')

    def test_basket_add_multiple_custom_product(self):
        basket = bskt.Basket()
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_TWO)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 2, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(154, currency='GBP'), 'Basket total incorrect')

    def test_basket_edit_custom_product(self):
        basket = bskt.Basket()
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        key = basket._encode_order_item_key(0, 0)

        basket.edit(order_item_key=key, new_customisation=EXAMPLE_CUSTOMISATION_THREE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(77, currency='GBP'), 'Basket total incorrect')

        edited_item = basket.get_product_customisation(order_product_index=0, customisation_index=0)
        self.assertEqual(edited_item.entry_class, 'Class 2101', 'Customisation name does not match expected.')

    def test_basket_remove_custom_product(self):
        basket = bskt.Basket()
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        key = basket._encode_order_item_key(0, 0)

        basket.remove(order_item_key=key)

        self.assertEqual(len(basket), 0, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 0, 'Basket item count incorrect')


class DiscountTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_basket_add_multiple_custom_products_with_fee(self):
        modifiers = mods.ItemModifiers(discounts=None, minimum_fees=[price_mods.MealKitFee()], surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_TWO)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 2, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(154, currency='GBP'), 'Basket total incorrect')

    def test_basket_add_multiple_custom_products_with_fee_ukvat(self):
        modifiers = mods.ItemModifiers(discounts=None, minimum_fees=[price_mods.MealKitFee()], surcharges=None, tax=price_mods.UKVAT())
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_TWO)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 2, 'Basket item count incorrect')
        total = basket.get_total()
        self.assertEqual(total, baseprice.Price(net=154, gross=184.8, currency='GBP'), 'Basket total incorrect')

    def test_basket_add_multiple_different_custom_products_with_fee(self):
        modifiers = mods.ItemModifiers(discounts=None, minimum_fees=[price_mods.MealKitFee()], surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)
        basket.add(product=inventory.get_inventory_item('3301'), customisation=EXAMPLE_CUSTOMISATION_TWO)

        self.assertEqual(len(basket), 2, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 2, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(154, currency='GBP'), 'Basket total incorrect')

    def test_basket_small_company_members_discount(self):
        modifiers = mods.ItemModifiers(discounts=[price_mods.SmallCompanyMembersDiscount()], minimum_fees=None, surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(33, currency='GBP'), 'Basket total incorrect')

    def test_basket_large_company_members_discount(self):
        modifiers = mods.ItemModifiers(discounts=[price_mods.LargeCompanyMembersDiscount()], minimum_fees=None, surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(43, currency='GBP'), 'Basket total incorrect')

    def test_basket_small_company_discount(self):
        modifiers = mods.ItemModifiers(discounts=[price_mods.SmallCompanyDiscount()], minimum_fees=None, surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(44, currency='GBP'), 'Basket total incorrect')

    def test_basket_supermarket_own_label_fee(self):
        modifiers = mods.ItemModifiers(discounts=None, minimum_fees=[price_mods.SupermarketOwnBrandFee()], surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101-smob'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(155, currency='GBP'), 'Basket total incorrect')

    def test_basket_multiple_minimum_fees(self):
        modifiers = mods.ItemModifiers(discounts=None, minimum_fees=[price_mods.MealKitFee(), price_mods.SupermarketOwnBrandFee()], surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101-smob'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(155, currency='GBP'), 'Basket total incorrect')

    def test_basket_multiple_discounts(self):
        modifiers = mods.ItemModifiers(discounts=[price_mods.LargeCompanyMembersDiscount(), price_mods.SmallCompanyDiscount(), price_mods.SmallCompanyMembersDiscount()], minimum_fees=None, surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        # TODO: check the price history to match the discunt names
        self.assertEqual(basket.get_total(), baseprice.Price(33, currency='GBP'), 'Basket total incorrect')

    def test_basket_discount_min_fee(self):
        modifiers = mods.ItemModifiers(discounts=[price_mods.SmallCompanyMembersDiscount()], minimum_fees=[price_mods.MealKitFee()], surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('3301'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(77, currency='GBP'), 'Basket total incorrect')

    def test_basket_multiple_discount_multiple_min_fee(self):
        modifiers = mods.ItemModifiers(discounts=[price_mods.SmallCompanyDiscount(), price_mods.SmallCompanyMembersDiscount()], minimum_fees=[price_mods.MealKitFee(), price_mods.SupermarketOwnBrandFee()], surcharges=None, tax=None)
        basket = bskt.Basket(item_modifiers=modifiers)
        basket.add(product=inventory.get_inventory_item('2101-smob'), customisation=EXAMPLE_CUSTOMISATION_ONE)

        self.assertEqual(len(basket), 1, 'Unique product count incorrect')
        self.assertEqual(basket.count(), 1, 'Basket item count incorrect')
        self.assertEqual(basket.get_total(), baseprice.Price(155, currency='GBP'), 'Basket total incorrect')

    # TODO: test multiple discounts of same value
