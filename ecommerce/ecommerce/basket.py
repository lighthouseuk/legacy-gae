__author__ = 'Matt Badger'

from satchless.item import Item as satchlessitem
from satchless.cart import Cart as satchlesscart
from satchless.cart import CartLine as satchlesscartline
from .base_modifiers import ItemModifiers
from .base_item import CustomisableItem, CustomisableStockedItem
import datetime


class PatchedCartLine(satchlesscartline):
    def get_quantity(self, **kwargs):
        return self.quantity


class CustomisableCartLine(satchlesscartline):
    _cache_get_total = None
    modified = False

    def __init__(self, product, quantity=0):
        self.customisations = []

        super(CustomisableCartLine, self).__init__(product=product, quantity=quantity, data=None)

    def __eq__(self, other):
        if not isinstance(other, CustomisableCartLine):
            return NotImplemented

        return (self.product == other.product and
                self.quantity == other.quantity and
                self.customisations == other.customisations)

    def __repr__(self):
        return 'CustomisableCartLine(product=%r, quantity=%r, customisations=%r)' % (self.product, self.quantity, self.customisations)

    def __getstate__(self):
        return (self.product, self.quantity, self.customisations)

    def __setstate__(self, data):
        self.product, self.quantity, self.customisations = data

    def get_quantity(self, **kwargs):
        return self.quantity

    def get_customisation_id(self, customisation):
        return self.customisations.index(customisation)

    def add_customised(self, customisation):
        # TODO: check customisation type?
        self.customisations.append(customisation)
        self.update_quantity()
        self.modified = True

    def delete_customised(self, customisation_id):
        del self.customisations[customisation_id]
        self.update_quantity()
        self.modified = True

    def edit_customised(self, order_item_id, customisation):
        self.customisations[order_item_id] = customisation

    def update_quantity(self):
        self.quantity = len(self.customisations)
        self.modified = True

    def get_total(self, force_reload=False, **kwargs):
        # TODO: add in ability to apply item modifiers to the entire cart line e.g. buy 5 for the price of 4
        # This may involve not calculating tax per item upfront but on the basket as a whole. Have an 'tax exempt' flag?

        if self._cache_get_total and not self.modified and not force_reload:
            return self._cache_get_total
        else:
            total = self.get_price_per_item(**kwargs) * self.get_quantity()
            self._cache_get_total = total
            self.modified = False
            return total


class Basket(satchlesscart):
    # TODO: method to filter a list of price modifiers based on config data
    # TODO: method to setup price modifiers for cart
    def __init__(self, items=None, item_modifiers=None):
        self._promo_codes = []
        if item_modifiers and isinstance(item_modifiers, ItemModifiers):
            self.item_modifiers = item_modifiers
        else:
            self.item_modifiers = None
        super(Basket, self).__init__(items=items)

    def __getstate__(self):
        # return a tuple as __getstate__ *must* return a truthy value
        out_state = {
            'state': self._state,
            'promo': self._promo_codes,
            'mods': self.item_modifiers,
        }
        return out_state

    def __setstate__(self, state):
        self._state = state['state']
        self._promo_codes = state['promo']
        self.item_modifiers = state['mods']
        self.modified = False

    def get_product_customisation(self, basket_index, customisation_index):
        product_line = self.get_product_line(order_product_index=basket_index)

        if not isinstance(product_line, CustomisableCartLine):
            raise TypeError('Cart line is not of type CustomisableCartLine')

        return product_line.customisations[customisation_index]

    def get_product_line(self, order_product_index):
        return self._state[order_product_index]

    def create_line(self, product, quantity, data):
        return PatchedCartLine(product, quantity, data=data)

    @staticmethod
    def create_customised_line(product, quantity=0):
        return CustomisableCartLine(product=product, quantity=quantity)

    def get_customisable_line(self, product):
        return next(
            (cart_line for cart_line in self._state
             if cart_line.product == product),
            None)

    def _get_or_create_customisable_line(self, product):
        cart_line = self.get_customisable_line(product)
        if cart_line:
            return (False, cart_line)
        else:
            return (True, self.create_customised_line(product))

    @staticmethod
    def decode_order_item_key(basket_item_key):
        basket_index, customisation_index = basket_item_key.split('-')
        return int(basket_index), int(customisation_index)

    @staticmethod
    def encode_order_item_key(basket_index, customisation_index):
        return '{0}-{1}'.format(basket_index, customisation_index)

    def add_normal_product(self, product, quantity=1, data=None, replace=False, check_quantity=True):
        created, cart_line = self._get_or_create_line(product, 0, data)

        if replace:
            new_quantity = quantity
        else:
            new_quantity = cart_line.quantity + quantity

        if new_quantity < 0:
            raise ValueError('%r is not a valid quantity (results in %r)' % (
                quantity, new_quantity))

        if check_quantity:
            self.check_quantity(product, new_quantity, data)

        cart_line.quantity = new_quantity

        if not cart_line.quantity and not created:
            self._state.remove(cart_line)
            self.modified = True
        elif cart_line.quantity and created:
            self._state.append(cart_line)
            self.modified = True
        elif not created:
            self.modified = True

    def add_customisable_product(self, product, customisation, quantity=1, check_quantity=True):
        created, cart_line = self._get_or_create_customisable_line(product)

        new_quantity = cart_line.quantity + quantity

        if quantity < 1:
            raise ValueError('%r is not a valid quantity to add (results in %r)' % (
                quantity, new_quantity))

        if check_quantity:
            self.check_quantity(product, new_quantity, None)

        cart_line.add_customised(customisation=customisation)

        if created:
            self._state.append(cart_line)

        self.modified = True

    def add(self, product, **kwargs):
        if isinstance(product, CustomisableItem) or isinstance(product, CustomisableStockedItem):
            self.add_customisable_product(product, **kwargs)
        elif isinstance(product, satchlessitem):
            self.add_normal_product(product, **kwargs)
        else:
            raise TypeError('Unrecognised product type')

    def edit_customisable_product(self, basket_item_key, new_customisation):
        basket_index, customisation_index = self.decode_order_item_key(basket_item_key=basket_item_key)

        cart_line = self.get_product_line(order_product_index=basket_index)

        if not isinstance(cart_line, CustomisableCartLine):
            raise TypeError('Cartline is not editable')

        cart_line.edit_customised(order_item_id=customisation_index, customisation=new_customisation)

        self.modified = True

    edit = edit_customisable_product

    def remove_customisable_product(self, basket_item_key):
        quantity = -1
        basket_index, customisation_index = self.decode_order_item_key(basket_item_key=basket_item_key)
        cart_line = self.get_product_line(order_product_index=basket_index)
        new_quantity = cart_line.quantity + quantity

        if new_quantity < 0:
            raise ValueError('%r is not a valid quantity to remove (results in %r)' % (
                quantity, new_quantity))

        if not new_quantity:
            del self._state[basket_index]
        else:
            cart_line.delete_customised(customisation_id=customisation_index)

        self.modified = True

    def remove(self, **kwargs):
        if kwargs.get('order_item_key', False):
            self.remove_customisable_product(**kwargs)
        elif kwargs.get('product', False) and isinstance(kwargs['product'], satchlesscartline):
            kwargs['quantity'] = -1
            self.add_normal_product(**kwargs)
        else:
            raise ValueError('Remove method not supported')

    def get_total(self, **kwargs):
        admin_override = getattr(self, 'admin_override', False)
        eval_date = getattr(self, 'payment_date', datetime.datetime.now())
        return super(Basket, self).get_total(item_modifiers=self.item_modifiers, promo_codes=self._promo_codes, force_reload=self.modified, admin_override=admin_override, eval_date=eval_date)

    def add_promo_code(self, promo_code):
        # TODO: change the promo code attr to be a set, not a list
        if not self._promo_codes:
            self._promo_codes = []
        self._promo_codes.append(promo_code)
        self.modified = True

    def remove_promo_code(self, promo_code):
        if not self._promo_codes:
            self._promo_codes = []
        self._promo_codes.pop(promo_code)
        self.modified = True

    def update_modifiers(self, modifiers):
        self.item_modifiers = modifiers
        self.modified = True

