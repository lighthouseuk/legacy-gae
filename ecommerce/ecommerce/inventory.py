__author__ = 'Matt Badger'


def default_builder(**kwargs):
    raise NotImplementedError()

try:
    import config.ecommerce_inventory as inventory
except ImportError:
    generate_item = default_builder
else:
    generate_item = inventory.build_entry_item
    generate_wca_item = inventory.build_wca_entry_item


def get_inventory_item(sku):
    return generate_item(sku)


def get_wca_inventory_item(sku):
    return generate_wca_item(sku)
