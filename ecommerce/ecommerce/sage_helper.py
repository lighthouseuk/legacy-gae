__author__ = 'Matt Badger'


def eu_country(country_code):
    eu_list = {'BE',
               'BG',
               'CZ',
               'DK',
               'DE',
               'EE',
               'IE',
               'GR',
               'ES',
               'FR',
               'HR',
               'IT',
               'CY',
               'LV',
               'LT',
               'LU',
               'HU',
               'MT',
               'NL',
               'AT',
               'PL',
               'PT',
               'RO',
               'SI',
               'SK',
               'FI',
               'SE',
               'GB'}
    check = {country_code}
    if len(eu_list - check) < 28:
        return True
    else:
        return False


def get_sage_product_code(product_sku, customer):
    eu = eu_country(customer.country)
    if customer.country == 'GB':
        return 'GT-UK'
    elif customer.tax_number and customer.tax_number != '' and eu:
        return 'GT-EU-VAT'
    elif eu:
        return 'GT-EU-NONVAT'
    else:
        return 'GT-ROW'


def get_sage_product_code_from_order(order):
    eu = eu_country(order.billing_address.country)
    if order.billing_address.country == 'GB':
        return 'GT-UK'
    elif order.customer.tax_number and order.customer.tax_number != '' and eu:
        return 'GT-EU-VAT'
    elif eu:
        return 'GT-EU-NONVAT'
    else:
        return 'GT-ROW'


def apply_sage_product_codes_to_basket(basket, customer):
    for line in basket:
        sage_code = get_sage_product_code(line.product.sku, customer)
        setattr(line, 'sage_sku', sage_code)

    return basket