__author__ = 'Matt Badger'

import datetime
import random
import string

try:
    import ecommerce.ecommerce.basket as bskt
    import ecommerce.ecommerce.basket as base_modifiers
except ImportError:
    import extlib.ecommerce.basket as bskt
    import extlib.ecommerce.basket as base_modifiers

try:
    import config.ecommerce_price_modifiers as modifiers
except ImportError:
    modifiers = False


class Customer(object):
    modified = False
    user_id = None
    company_id = None
    company_name = None
    first_name = None
    last_name = None
    email = None
    phone = None
    turnover_ltm = False
    tax_number = None
    membership_number = None

    def __init__(self, company_name=None, first_name=None, last_name=None, email=None, phone=None, turnover_ltm=False, tax_number=None, membership_number=None, user_id=None, company_id=None):
        self.modified = False
        self.user_id = user_id
        self.company_id = company_id
        self.company_name = company_name
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.phone = phone
        self.turnover_ltm = turnover_ltm
        self.tax_number = tax_number if tax_number else False
        self.membership_number = membership_number if membership_number else False

    def __setattr__(self, name, value):
        if hasattr(self, name) and name != 'modified':
            if getattr(self, name) != value:
                self.modified = True
            super(Customer, self).__setattr__(name, value)
        elif hasattr(self, name):
            super(Customer, self).__setattr__(name, value)
        else:
            raise AttributeError('\'{0}\' attribute is not supported'.format(name))

    def validate(self):
        valid = True

        if not self.first_name:
            valid = False

        if not self.last_name:
            valid = False

        if not self.email:
            valid = False

        return valid

    def reset_modified(self):
        self.modified = False


class Address(object):
    modified = False
    address_1 = None
    address_2 = None
    city = None
    country = None
    state = None
    post_code = None

    def __init__(self, address_1=None, address_2=None, city=None, country=None, state=None, post_code=None):
        self.modified = False
        self.address_1 = address_1
        self.address_2 = address_2
        self.city = city
        self.country = country

        if country == 'US':
            self.state = 'WA'
        else:
            self.state = None

        if not post_code or len(post_code) < 3:
            self.post_code = '000'
        else:
            self.post_code = post_code

    def __setattr__(self, name, value):
        if hasattr(self, name) and name != 'modified':
            if getattr(self, name) != value:
                self.modified = True
            super(Address, self).__setattr__(name, value)
        elif hasattr(self, name):
            super(Address, self).__setattr__(name, value)
        else:
            raise AttributeError('\'{0}\' attribute is not supported'.format(name))

    def validate(self):
        valid = True

        if not self.address_1:
            valid = False

        if not self.city:
            valid = False

        if not self.country:
            valid = False

        return valid

    def reset_modified(self):
        self.modified = False


class Order(object):
    modified = False
    order_reference = None
    order_type = 'default'
    customer = None
    billing_address = None
    delivery_address = None
    basket = None
    total = None
    currency = None
    payment_status = None
    payment_method = None
    payment_date = None
    payment_completed_by = None
    payment_completed_by_name = None

    def __init__(self, customer=None, billing_address=None, delivery_address=None, basket=None):
        self.order_reference = self.generate_order_reference()

        if customer and isinstance(customer, Customer):
            self.customer = customer
        elif customer:
            self.customer = Customer(**customer)
        else:
            self.customer = Customer()

        if billing_address and isinstance(billing_address, Address):
            self.billing_address = billing_address
        elif billing_address:
            self.billing_address = Address(**billing_address)
        else:
            self.billing_address = Address()

        if delivery_address and isinstance(delivery_address, Address):
            self.delivery_address = delivery_address
        elif delivery_address:
            self.delivery_address = Address(**delivery_address)
        else:
            self.delivery_address = Address()

        if basket and isinstance(basket, bskt.Basket):
            self.basket = basket
        else:
            self.basket = bskt.Basket()

    def __getattr__(self, name):
        if name == 'basket':
            self.update_modifiers()

        super(Order, self).__getattribute__(name)

    def __setattr__(self, name, value):
        if hasattr(self, name) and name != 'modified':
            super(Order, self).__setattr__(name, value)
            self.modified = True
        elif hasattr(self, name):
            super(Order, self).__setattr__(name, value)
        else:
            raise AttributeError('\'{0}\' attribute is not supported'.format(name))

    @staticmethod
    def generate_order_reference():
        random_token = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(5)])
        return 'WCA15-{0}{1}'.format(random_token, datetime.datetime.now().strftime("%Y%m%d%H%M%S"))

    def update_order_reference(self):
        self.order_reference = self.generate_order_reference()

    def update_modifiers(self):
        reload = False

        if self.customer.modified:
            reload = True
            self.customer.reset_modified()

        if self.billing_address.modified:
            reload = True
            self.billing_address.reset_modified()

        if self.delivery_address.modified:
            reload = True
            self.delivery_address.reset_modified()

        # if reload or datetime.datetime.now() > datetime.datetime(year=2015, month=2, day=1, hour=23, minute=59, second=59):
        if reload:
            valid_mods = self._get_valid_modifiers_for_order(self)
            self.basket.update_modifiers(modifiers=valid_mods)

    def validate(self):
        valid = True

        if not self.order_reference:
            valid = False

        if not self.customer:
            valid = False
        elif self.customer and not self.customer.validate():
            valid = False

        if not self.billing_address:
            valid = False
        elif self.billing_address and not self.billing_address.validate():
            valid = False

        if not self.delivery_address:
            valid = False
        elif self.delivery_address and not self.delivery_address.validate():
            valid = False

        if not self.basket:
            valid = False
        elif self.basket and not self.basket.count():
            valid = False

        return valid

    @staticmethod
    def _get_valid_modifiers_for_order(order):
        if not modifiers:
            return []

        mod_dict = base_modifiers.ItemModifiers([], [], [], [])._asdict()

        for mod in modifiers.ACTIVE_MODIFIERS:
            if mod.applies_to_order(order=order):
                if mod.modifier_group == 'tax':
                    mod_dict[mod.modifier_group] = mod()
                else:
                    if not mod_dict.get(mod.modifier_group):
                        mod_dict[mod.modifier_group] = []

                    mod_dict[mod.modifier_group].append(mod())

        valid_mods = base_modifiers.ItemModifiers(**mod_dict)

        return valid_mods