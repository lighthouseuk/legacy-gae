__author__ = 'Matt Badger'

from satchless.item import Item as satchlessitem, StockedItem as satchlessstockeditem
from .base_price import Price


def compile_item_modifiers(item, item_modifiers, **kwargs):
    discounts = None
    minimum_fees = None
    surcharges = None
    tax = None

    if item_modifiers.discounts:
        calculated_discounts = []
        for discount in item_modifiers.discounts:
            if discount.applicable(item, **kwargs):
                calculated_discounts.append(discount)
        if calculated_discounts:
            discounts = max(calculated_discounts)

    if item_modifiers.minimum_fees:
        calculated_minimum_fees = []
        for minimum_fee in item_modifiers.minimum_fees:
            if minimum_fee.applicable(item, **kwargs):
                calculated_minimum_fees.append(minimum_fee)
        if calculated_minimum_fees:
            minimum_fees = max(calculated_minimum_fees)

    if item_modifiers.surcharges:
        for surcharge in item_modifiers.surcharges:
            if surcharge.applicable(item, **kwargs):
                if not surcharges:
                    surcharges = surcharge
                else:
                    surcharges += surcharge

    if item_modifiers.tax:
        tax = item_modifiers.tax

    return discounts, minimum_fees, surcharges, tax


class CustomisableItem(satchlessitem):
    price = Price(0, currency='GBP')
    _price_per_item_cache = None

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def get_price_per_item(self, item_modifiers=None, **kwargs):
        # if self._price_per_item_cache:
        #     return self._price_per_item_cache

        if not self.price:
            raise AttributeError('Item was initialised without a valid price.')

        price = self.price

        if item_modifiers:
            # Pass modifiers to function which checks if they apply to this product. yield discounts, surcharges, tax
            # Modifiers in a namedtuple?
            discount, minimum_fee, surcharge, tax = compile_item_modifiers(self, item_modifiers, **kwargs)
            if discount:
                price += discount

            if minimum_fee:
                price += minimum_fee

            if surcharge:
                price += surcharge

            if tax:
                price += tax

        # self._price_per_item_cache = price

        return price


class CustomisableStockedItem(satchlessstockeditem):
    price = Price(0, currency='GBP')
    _price_per_item_cache = None

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def get_price_per_item(self, item_modifiers=None, **kwargs):
        # if self._price_per_item_cache:
        #     return self._price_per_item_cache

        if not self.price:
            raise AttributeError('Item was initialised without a valid price.')

        price = self.price

        if item_modifiers:
            # Pass modifiers to function which checks if they apply to this product. yield discounts, surcharges, tax
            # Modifiers in a namedtuple?
            discount, minimum_fee, surcharge, tax = compile_item_modifiers(self, item_modifiers, **kwargs)
            if discount:
                price += discount

            if minimum_fee:
                price += minimum_fee

            if surcharge:
                price += surcharge

            if tax:
                price += tax

        # self._price_per_item_cache = price

        return price