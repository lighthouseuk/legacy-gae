__author__ = 'Matt Badger'

import prices


class Price(prices.Price):
    """
    Overriding the Price class in Satchless to coerce float values to strings before converting to Decimal.
    This ensures that the equality functions act as you would expect when working with currency.
    """
    def __new__(cls, net=None, gross=None, currency=None, history=None):
        if isinstance(net, float):
            net = str(net)
        if gross is not None and isinstance(gross, float):
            gross = str(gross)
        elif gross is None:
            gross = net
        return super(Price, cls).__new__(cls, net=net, gross=gross, currency=currency, history=history)

