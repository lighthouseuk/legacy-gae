__author__ = 'Matt Badger'

import unittest

try:
    import ecommerce.ecommerce.order as order
except ImportError:
    import extlib.ecommerce.order as order

# TODO: test admin override for item modifiers

# TODO: pass 'admin_discount_override' to get_total in order to override discounts to be valid


class OrderTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_setattr(self):
        test_order = order.Order()
        test_order.billing_address.address_1 = 'Test'
        test_order.update_modifiers()
        test_order.basket.count()